from django.urls import path
from projects.views import view_project, create_project, project_list

urlpatterns = [
    path("", project_list, name="list_projects"),
    path("<int:id>/", view_project, name="show_project"),
    path("create/", create_project, name="create_project"),
]
